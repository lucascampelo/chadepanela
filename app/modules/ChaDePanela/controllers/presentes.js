(function (chaDePanela) {
    var PresentesController = function ($rootScope, $scope, $timeout, $filter, ListaDePresentes) {
        /**
         * PresentesController
         */
        var self = this;

        /**
         * Presente escolhido na lista
         */
        var presenteSelecionado;

        /**
         * Posição na Lista do Presente escolhido
         */
        var indexPresenteSelecionado;

        /**
         * Presentes Especiais
         * @type {{nome: string, valor: string, tipo: string, presenteador: string, mostrar: string}[]||null}
         */
        $scope.presentesEspeciais = null;

        /**
         * Presentes Normais
         * @type {{nome: string, valor: string, tipo: string, presenteador: string, mostrar: string}[]||null}
         */
        $scope.presentesNormais = null;

        /**
         * Evento de Click do Presente (ngClick)
         * @param $event
         * @param {int} $index
         * @returns void
         */
        $scope.onClickPresente = function ($event, $index) {
            $event.originalEvent.preventDefault();

            if (!this.presente.presenteador) {
                presenteSelecionado = this.presente;
                indexPresenteSelecionado = $index;
                $scope.nomePresenteador = '';
                angular.element('#btnModal').click();
            }
        };

        /**
         * Evento disparado ao "salvar" a modal.
         * Salva o nome do presenteador no arquivo CSV
         * @param $event
         */
        $scope.onSubmitModal = function ($event) {
            ListaDePresentes.setPresenteador(presenteSelecionado.linha, $scope.nomePresenteador).success(function() {
                presenteSelecionado.presenteador = $scope.nomePresenteador;

                var input = angular.element('#presente-'+presenteSelecionado.tipo+'-'+indexPresenteSelecionado);
                if (input.length) {
                    input[0].checked = true;
                }

                angular.element('#btnModalClose').click();
            });
        };

        /**
         * Carrega os Presentes
         */
        var loadPresentes = function () {
            ListaDePresentes.load().success(function (response) {
                var presentes = $filter('groupBy')(response.items, 'tipo');

                $scope.presentesEspeciais = presentes.especial;
                $scope.presentesNormais = presentes.normal;
            }).error(function () {
                Materialize.toast("Um erro ocorreu ao mostrar a lista de presentes.", 5000, "red");
            });
        };

        var blurBackgroundHeader = function() {
            var img = angular.element('.parallax-container img');
            if (img.length) {
                img.addClass('blur');
            }
        };

        loadPresentes();

        $timeout(function () {
            blurBackgroundHeader();
        }, 1000);
    };

    chaDePanela.controller("Presentes", PresentesController);
}(chaDePanela));