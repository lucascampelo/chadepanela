(function (chaDePanela) {
    var ListaDePresentes = function ($http) {
        var self = this,
            newSheetUrl = 'api/';

        /**
         * Carrega os presentes
         */
        self.load = function () {
            return $http.get(newSheetUrl, {cache: true});
        };

        /**
         * Salva o nome do presenteador no arquivo CSV
         * @param {int} rowNumber Linha no CSV
         * @param {string} presenteador Nome do Presenteador
         */
        self.setPresenteador = function(rowNumber, presenteador) {
            return $http({
                method: 'POST',
                url: newSheetUrl,
                data: $.param({linha: rowNumber, presenteador: presenteador}),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
        };

    };

    chaDePanela.service('ListaDePresentes', ListaDePresentes);
}(chaDePanela));