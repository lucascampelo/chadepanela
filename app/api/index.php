<?php

DEFINE('DS', DIRECTORY_SEPARATOR);
DEFINE('ROOT', dirname(__FILE__));
DEFINE('CSV_FILE', ROOT . DS . 'data' . DS . 'lista-de-presentes.csv');

require 'functions.php';
require_once 'vendor' . DS . 'autoload.php';

try {
    $method = $_SERVER['REQUEST_METHOD'];

    switch ($method) {
        case 'POST':
            if (array_key_exists('linha', $_POST) && !empty($_POST['linha']) &&
                array_key_exists('presenteador', $_POST) && !empty($_POST['presenteador'])) {
                $arrayReturn = setRow($_POST['linha'], $_POST['presenteador']);
            } else {
                header($_SERVER['SERVER_PROTOCOL'] . ' 412 Precondition Failed', true, 412);
                $arrayReturn = [
                    'error' => 1,
                    'message' => 'Os campos "linha" e "presenteador" são obrigatórios.'
                ];
            }
            break;

        default:
        case 'GET':
            $arrayReturn = getRows();
            break;
    }

    responseJson($arrayReturn);
} catch (Exception $e) {
    # Cabeçalhos de Resposta
    header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
    header('Content-Type: application/json;');

    # Resposta de Erro
    $jsonReturnError = [
        'error' => true,
        'message' => 'Um erro ocorreu ao recuperar as informações.',
        'debug' => $e->getMessage()
    ];

    # Imprime o resultado
    echo json_encode($jsonReturnError);
}