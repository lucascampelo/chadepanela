<?php

use League\Csv\Reader;
use League\Csv\Writer;

if (!ini_get("auto_detect_line_endings")) {
    ini_set("auto_detect_line_endings", '1');
}

$rowKeys = [
    0 => 'nome',
    1 => 'valor',
    2 => 'tipo',
    3 => 'presenteador',
    4 => 'mostrar'
];

/**
 * Converte uma linha do CSV para uma linha com nomes associativos
 * @param array $row Linha do arquivo CSV
 * @param int $index Número da Linha
 * @return array Linha com as colunas nomeadas
 */
function parseRow($row, $index)
{
    global $rowKeys;
    $rowIndex = ['linha' => $index];
    return array_merge($rowIndex, array_combine($rowKeys, $row));
}

/**
 * Responde a requisição com um JSON
 * @param array $arrayReturn Retorno que será transformado em JSON
 */
function responseJson($arrayReturn)
{
    header("Content-Type: application/json;");
    echo json_encode($arrayReturn);
}

/**
 * Retorna o Objeto de Manipulação do CSV
 * @return League\Csv\Reader
 */
function getCsvReader()
{
    return Reader::createFromPath(CSV_FILE);
}

/**
 * Retorna a Lista de Presentes em formato de Array
 * @return array
 */
function getRows()
{
    $reader = getCsvReader();
    $items = [];
    foreach ($reader->getIterator() as $index => $row) {
        if ($index > 0) {
            $items[] = parseRow($row, $index);
        }
    }

    return [
        'items' => $items,
        'total_items' => count($items)
    ];
}

/**
 * Define o presenteador para uma linha específica
 * @param $rowNumber
 * @param $presenteador
 * @return array
 */
function setRow($rowNumber, $presenteador)
{
    $col = 3;
    $reader = getCsvReader()->setOffset($rowNumber)->setLimit(1);
    $total = iterator_count($reader->getIterator()) - 1;

    if ($rowNumber > 0 && $rowNumber <= $total) {
        $newCsv = [];

        foreach ($reader->getIterator() as $rowIndex => $row) {
            if ($rowIndex == $rowNumber) {
                $row[$col] = $presenteador;
            }
            $newCsv[$rowIndex] = $row;
        }

        $writer = Writer::createFromPath(CSV_FILE);
        $writer->insertAll($newCsv);

        return [
            'error' => 0,
            'message' => 'Alterado com sucesso'
        ];
    }

    return [
        'error' => 1,
        'message' => sprintf('Linha fora do permitido (entre 1 e %d)', $total)
    ];
}